package calcx;

public class FloatToken extends Token {
    public float floatValue;

    public FloatToken(float value) {
        super(TokenType.FLOAT);
        floatValue = value;
    }
    
    public float getValue() {
        return floatValue;
    }
    
    public void print() {
        System.out.println("Float Token: " + floatValue);
    }
}