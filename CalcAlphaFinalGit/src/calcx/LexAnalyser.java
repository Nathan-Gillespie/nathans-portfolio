
package calcx;

import java.io.IOException;
import java.util.Hashtable;
import java.util.ArrayList;

public class LexAnalyser {
	public int lineNumber = 1;
	public boolean complete = true;
	private char peek = ' ';
	private Hashtable<String, VarToken> identifiers;
	public ArrayList<Token> tokenSequence = new ArrayList<Token>();
	int currentToken = 0;

	// Constructor for LexAnalyser class
	public LexAnalyser() {
		identifiers = new Hashtable<String, VarToken>();
	}

	// This next method will add a variable identifier token to the hashtable
	public void addIdentifier(VarToken token) {
		identifiers.put(token.getIdName(), token);
	}

	// This method tries to find a variable identifier token of the given name
	public VarToken findIdentifier(String identifierName) {
		return identifiers.get(identifierName);
	}

	// This method returns the hashtable of all identifiers
	public Hashtable<String, VarToken> getIdentifiers() {
		return identifiers;
	}

	// This method returns the sequence of tokens
	public ArrayList<Token> getTokens() {
		return tokenSequence;
	}

	/*
	 * This next method will read in a single integer value from user input and
	 * return it
	 */
	public int readInt() throws IOException {
		if (Character.isDigit(peek)) {
			int readInt = 0;
			do {
				readInt = 10 * readInt + Character.digit(peek, 10);
				peek = (char) System.in.read();
			} while (Character.isDigit(peek));
			return readInt; // readInt stores the integer value of the token
		} else {
			System.out.println("Error.");
			return 0;
		}
	}

	public float readFloat() throws IOException {
		if (Character.isDigit(peek)) {
			float readFloat = 0;
			do {
				readFloat = 10 * readFloat + Character.digit(peek, 10);
				peek = (char) System.in.read();
			} while (Character.isDigit(peek));
			return readFloat; // readInt stores the integer value of the token
		} else {
			System.out.println("Error.");
			return 0;
		}
	}

	public char readChar() throws IOException {
		char b = (char) System.in.read();
		return b;
	}

	/*
	 * This next method will read in the next string value and return it
	 */
	public String readString() throws IOException {
		StringBuffer b = new StringBuffer();
		do {
			b.append(peek);
			peek = (char) System.in.read();
		} while (Character.isLetterOrDigit(peek));
		String s = b.toString(); // s now stores the next lexeme as a string
		return s;
	}

	/*
	 * This next method removes whitespace (newlines, carriage returns, tabs or
	 * spaces)
	 */
	public void removeWhitespace() throws IOException {
		for (;; peek = (char) System.in.read()) {
			if (peek == ' ' || peek == '\t' || peek == '\r')
				continue;
			else if (peek == '\n')
				lineNumber = lineNumber + 1;
			else
				break;
		}
	}

	// This method adds a given token to the array of tokens
	public void addTokenToArray(Token tokenToAdd) {
		tokenSequence.add(tokenToAdd);
		currentToken++;
	}

	/*
	 * This next method performs lexical analysis by continually reading in lexemes
	 * and storing them in an array of Tokens tokenSequence. The method returns once
	 * it finds an error or sees the end of file symbol '$'. scan() returns the
	 * number of tokens found.
	 */
	public Analysis scan() throws IOException {
		Token nextToken;
		do {
			nextToken = new Token(TokenType.NULL_TOKEN); // Initialise nextToken to have type NULL

			// First we may read characters until end of whitespace (spaces, tabs or
			// newlines)...
			removeWhitespace();

			// Now determine what type of token we have and add it to the array

			// Check if next lexeme is a number (beginning with a digit)
			if (Character.isDigit(peek)) {
				int readValue = readInt();
				if (Character.valueOf('.') == peek) {
					String floatStr = Integer.toString(readValue) + ".";
					peek = ' ';
					removeWhitespace();
					int after = readInt();
					floatStr += Integer.toString(after);
					float floatVal = Float.parseFloat(floatStr);
					nextToken = new FloatToken(floatVal);
					addTokenToArray(nextToken);
					continue;
				}
				nextToken = new NumToken(readValue);
				addTokenToArray(nextToken);
				continue; // Restart the loop to find next token
			}

			// Otherwise, check if the next token is a valid identifier or else a keyword
			else if (Character.isLetter(peek)) {
				String nextWord = readString();

				if (nextWord.equals("INT")) {
					// s represents an INT keyword
					nextToken = new Token(TokenType.INT_KEYWORD);
					addTokenToArray(nextToken);
					// Since this is an INT keyword, the next token must be an integer variable
					removeWhitespace();
//					if (!(Character.isLetter(peek))) {
//						System.out.println("Expected a variable name during INT declaration.");
//						complete = false;
//						break;
//					}
					nextWord = readString();
					nextToken = new IntVarToken(nextWord);
					addTokenToArray(nextToken);
					addIdentifier((IntVarToken) nextToken);
					continue;
				} else if (nextWord.equals("FLOAT")) {
					nextToken = new Token(TokenType.FLOAT_KEYWORD);
					addTokenToArray(nextToken);
					removeWhitespace();
					nextWord = readString();
					nextToken = new FloatVarToken(nextWord);
					addTokenToArray(nextToken);
					addIdentifier((FloatVarToken) nextToken);
					continue;
				} else if (nextWord.equals("f")) {
					nextToken = new Token(TokenType.END_OF_FLOAT);
					addTokenToArray(nextToken);
					removeWhitespace();
				} else {
					nextToken = findIdentifier(nextWord);
					if (nextToken != null) {
						addTokenToArray(nextToken);
						continue;
					} else {
						System.out.println("Identifier not found.");
						nextToken = new Token(TokenType.NULL_TOKEN);
						break; // Stop lexical analysis
					}
				}
			}
			// Check if the next token an operator or end of line/end of file symbol
			switch (peek) {
			case (';'):
				peek = ' ';
				nextToken = new Token(TokenType.SEMICOLON);
				addTokenToArray(nextToken);
				break; // Leave the switch statement (not the do..while loop)
			case ('+'):
				peek = ' ';
				nextToken = new Token(TokenType.PLUS);
				addTokenToArray(nextToken);
				break;
			case ('-'):
				peek = ' ';
				nextToken = new Token(TokenType.MINUS);
				addTokenToArray(nextToken);
				break;
			case (':'):
				peek = ' ';
				nextToken = new Token(TokenType.COLON);
				addTokenToArray(nextToken);
				break;
			case ('='):
				peek = ' ';
				nextToken = new Token(TokenType.EQUALS);
				addTokenToArray(nextToken);
				break;
			case ('*'):
				peek = ' ';
				nextToken = new Token(TokenType.MULTIPLY);
				addTokenToArray(nextToken);
				break;
			case ('/'):
				peek = ' ';
				nextToken = new Token(TokenType.DIVIDE);
				addTokenToArray(nextToken);
				break;
			case ('~'):
				peek = ' ';
				nextToken = new Token(TokenType.DIFFERENCE);
				addTokenToArray(nextToken);
				break;
			case ('^'):
				peek = ' ';
				nextToken = new Token(TokenType.POWER);
				addTokenToArray(nextToken);
				break;
			case ('%'):
				peek = ' ';
				nextToken = new Token(TokenType.MODULUS);
				addTokenToArray(nextToken);
				break;
			case ('$'):
				peek = ' ';
				nextToken = new Token(TokenType.END_OF_FILE);
				addTokenToArray(nextToken);
				break;
			default:
				nextToken = new Token(TokenType.NULL_TOKEN);
				System.out.println("Unknown token.");
			}
		} while (nextToken.returnType() != TokenType.NULL_TOKEN && nextToken.returnType() != TokenType.END_OF_FILE);

		if (nextToken.returnType() == TokenType.NULL_TOKEN || !complete) {
			System.out.println("Lexical analysis unsuccessful.");
			complete = false;
			Analysis analysis = new Analysis(currentToken, complete);
			return analysis; // To indicate an error
		}  
		else {
			System.out.println("Lexical analysis successful with " + currentToken + " tokens.");
			complete = true;
			Analysis analysis = new Analysis(currentToken, complete);
			return analysis;
		}
		
	}

}
