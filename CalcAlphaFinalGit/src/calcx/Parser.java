
package calcx;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Hashtable;

public class Parser {
    public ArrayList<Token> tokens;  // Array of tokens to be parsed
    public int position;    // Current position in array
    
    Hashtable<String, VarToken> vars;         // Hashtable of all identifiers
    
    // Constructor for Parser class. Sequence of tokens and a hashtable of identifiers should be passed. 
    public Parser(ArrayList<Token> tokenSeq, Hashtable<String, VarToken> variables) {
        tokens = tokenSeq;  
        position = 0;       // Current position in sequence
        vars = variables;
    }
     
    // This method is called when a parse error occurs on some token
    private void parseError() {
   	 	tokens.get(position).print();
        System.out.println("Parse Error"); 
    }
    
    private void expectedError(TokenType tokType) {
   	 	tokens.get(position).print();
        System.out.printf("Parse Error. Expected %s token", tokType); 
    }
    
    
    // This method checks if the next token is of type tokType and if so moves on to the next token 
    private void match(TokenType tokType) {
        if(tokens.get(position).returnType() != tokType) {
    		System.out.printf("Token %s : ", position+1);
    		expectedError(tokType);
    		System.exit(0);
        }
        position++;
    }
    
    // This method determines if the next token is of type tokType (returning a boolean)  
    private boolean check(TokenType tokType) {
        if(tokens.get(position).returnType() != tokType) {
            return false;
        }
        return true;
    }
    
    /*
     * These next methods implement the Context Free Grammar / Syntax Directed Translation Scheme. There is a method
     * for each nonterminal of the grammar and production rules determine the operations to be carried out.
     */
    
    // Start to parse the program
    public void prog() {
        // First parse declarations
        decls();
		// Next parse colon
        match(TokenType.COLON);
        // Next parse expression
        float value = expr();
        // Finally parse the end of file character
        match(TokenType.END_OF_FILE);
        BigDecimal strippedVal = new BigDecimal(Float.toString(value)).stripTrailingZeros();
        System.out.println("Value of expression is: " + (strippedVal.toPlainString()));
    }
    
    public void decls() {
    	if(check(TokenType.INT_KEYWORD) || check(TokenType.FLOAT_KEYWORD)) {
    		decl();
    		decls();
    	}
    	else {
    		// Do nothing, epsilon production in CFG
    	}
    }
    
    public void decl() {    	
    	if(check(TokenType.INT_KEYWORD)) {
    		match(TokenType.INT_KEYWORD);
    		IntVarToken nextIntVar = (IntVarToken) tokens.get(position);
    		match(TokenType.INT_VAR);
    		match(TokenType.EQUALS);
    		NumToken nextInteger = (NumToken) tokens.get(position);
    		match(TokenType.NUM);
    		match(TokenType.SEMICOLON);
    		// Now do the semantic action
    		nextIntVar.setValue(nextInteger.getValue());
    	} 
    	else if(check(TokenType.FLOAT_KEYWORD)) {
    		match(TokenType.FLOAT_KEYWORD);
    		FloatVarToken nextFloatVar = (FloatVarToken) tokens.get(position);
    		match(TokenType.FLOAT_VAR);
    		match(TokenType.EQUALS);
    		FloatToken nextFloat = (FloatToken) tokens.get(position);
    		match(TokenType.FLOAT);
    		match(TokenType.END_OF_FLOAT);
    		match(TokenType.SEMICOLON);
    		// Now do the semantic action
    		nextFloatVar.setValue(nextFloat.getValue());
    	} 
    	else {
    		parseError();
    	}
    }
    
    public float expr() {
    	float value = 0;
    	if(tokens.get(position).returnType() == TokenType.INT_VAR) {
    		value = ((IntVarToken) tokens.get(position)).getValue();
    		match(TokenType.INT_VAR);
    	}
    	else if(tokens.get(position).returnType() == TokenType.NUM) {
    		value = ((NumToken) tokens.get(position)).getValue();
    		match(TokenType.NUM);
    	}
    	else if(tokens.get(position).returnType() == TokenType.FLOAT_VAR) {
    		value = ((FloatVarToken) tokens.get(position)).getValue();
    		match(TokenType.FLOAT_VAR);
    	}
    	else {
    		parseError();
    	}
   		switch(tokens.get(position).returnType()) {
    		case PLUS : op(); float value2 = expr(); value = value + value2; break; // Semantic action 
    		case MINUS: op(); float value2b = expr(); value = value - value2b; break; // Semantic action
    		case MULTIPLY: op(); float value2c = expr(); value = value * value2c; break; // Semantic action 
    		case DIVIDE: op(); float value2d = expr(); value = value / value2d; break; // Semantic action 
    		case POWER: op(); float value2e = expr(); value = ((float)Math.pow(value, value2e)); break; // Semantic action 
    		case DIFFERENCE: op(); float value2f = expr(); value = Math.abs(value - value2f); break; // Semantic action 
    		case MODULUS: op(); float value2g = expr(); value = value % value2g; break; // Semantic action 
    		default:
    	}
    	return value;
    }
    
    public void op() {
    	if(check(TokenType.MINUS)) {
    		match(TokenType.MINUS);
    	}
    	else if(check(TokenType.PLUS)) {
    		match(TokenType.PLUS);
    	}
    	else if(check(TokenType.MULTIPLY)) {
    		match(TokenType.MULTIPLY);
    	}
    	else if(check(TokenType.DIVIDE)) {
    		match(TokenType.DIVIDE);
    	}
    	else if(check(TokenType.POWER)) {
    		match(TokenType.POWER);
    	}
    	else if(check(TokenType.DIFFERENCE)) {
    		match(TokenType.DIFFERENCE);
    	}
    	else if (check(TokenType.MODULUS)) {
    		match(TokenType.MODULUS);
    	}
    	else {
    		parseError(); // op should be "+", "-", "*", "/", "^" or "~"
    	}
    }
}
