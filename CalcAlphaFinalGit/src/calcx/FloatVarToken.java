package calcx;

public class FloatVarToken extends VarToken {
    public float value;
    
    public FloatVarToken(String identName) {
        super(identName, TokenType.FLOAT_VAR);
        value = 0;
    }
    
    public float getValue() {
        return value;
    }
        
    public void setValue(float newValue) {
        value = newValue;
    }
    
    public void print() {
        System.out.println("Float Variable Token: " + identifierName);
    }
}
