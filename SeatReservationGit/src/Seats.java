
public class Seats {
	String seatNum;
	String seatType;
	boolean window;
	boolean aisle;
	boolean table;
	double seatPrice;
	String email;

	Seats(String num, String type, boolean window, boolean aisle, boolean table, double price, String email) {
		this.seatNum = num;
		this.seatType = type;
		this.window = window;
		this.aisle = aisle;
		this.table = table;
		this.seatPrice = price;
		this.email = email;
	}
	
	public String toString () {
		return(seatNum+" "+seatType+" "+window+" "+aisle+" "+table+" "+seatPrice+" "+email);
	}
}
