import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class SeatReservation {

	public static void main(String[] args) throws IOException {

		Scanner console = new Scanner(System.in);

		String seatsFile = "src//seats.txt";
		FileReader file = new FileReader(seatsFile);
		Scanner read = new Scanner(file);

		ArrayList<Seats> allSeats = new ArrayList<Seats>();
		ArrayList<Seats> availableSeats = new ArrayList<Seats>();
		ArrayList<Seats> reservedSeats = new ArrayList<Seats>();

		Seats newSeat = null;
		do {
			newSeat = new Seats(read.next(), read.next(), read.nextBoolean(), read.nextBoolean(), read.nextBoolean(),
					read.nextDouble(), read.next());
			allSeats.add(newSeat);
		} while (read.hasNext());
		read.close();

		String userEmail;
		do {
			System.out.println("Please enter your E-mail: ");
			userEmail = Choice(console);
		} while (!(userEmail.contains("@") && (userEmail.contains(".com") || userEmail.contains(".co.uk"))));

		Menu(console, allSeats, availableSeats, reservedSeats, userEmail, seatsFile);
	}

	public static void Menu(Scanner console, ArrayList<Seats> allSeats, ArrayList<Seats> availableSeats,
			ArrayList<Seats> reservedSeats, String userEmail, String seatsFile) throws IOException {
		char pound = '�';
		System.out.println("-- Seat Booking System --\n" + "\n-- MAIN MENU --\n" + "1 - Reserve Seat\n"
				+ "2 - Cancel Seat\n" + "3 - View Seat Reservation\n" + "Q - Quit\n");
		boolean menuVal = false;
		do {
			System.out.println("Please enter the character corresponding to the desired option above: ");
			String menuChoice = Choice(console);

			if (menuChoice.trim().equals("1")) {
				System.out.println("-- RESERVE SEAT --\n");
				Reserve(allSeats, availableSeats, reservedSeats, userEmail, console, pound, seatsFile);
				menuVal = true;
			}
			if (menuChoice.trim().equals("2")) {
				Cancel(allSeats, availableSeats, reservedSeats, userEmail, pound, console, seatsFile);
				menuVal = true;
			}
			if (menuChoice.trim().equals("3")) {
				View(allSeats, availableSeats, reservedSeats, console, userEmail, pound, seatsFile);
				menuVal = true;
			}
			if (menuChoice.trim().toLowerCase().equals("q")) {
				Quit(allSeats, seatsFile);
				menuVal = true;
			}
		} while (!menuVal);
	}

	public static void Reserve(ArrayList<Seats> allSeats, ArrayList<Seats> availableSeats,
			ArrayList<Seats> reservedSeats, String userEmail, Scanner console, char pound, String seatsFile)
			throws IOException {
		ArrayList<Seats> searchSeats = new ArrayList<Seats>();
		String reserveYN;
		boolean reserveY = false;
		availableSeats.clear();
		availableSeats = AvailableSeats(allSeats, availableSeats, pound);
		while (availableSeats.size() > 0) {
			availableSeats.clear();
			availableSeats = AvailableSeats(allSeats, availableSeats, pound);

			do {
				System.out
						.println("Would you like to reserve a seat?(Y/N)?\n(If no, you will be returned to the menu)");
				reserveYN = Choice(console);
				if (reserveYN.toUpperCase().startsWith("N")){
					Menu(console, allSeats, availableSeats, reservedSeats, userEmail, seatsFile);
				}
				if (reserveYN.toUpperCase().startsWith("Y")){
					reserveY = true;
				}
			} while (!(reserveY));

			if (reserveYN.toUpperCase().startsWith("N")) {
				try {
					Menu(console, allSeats, availableSeats, reservedSeats, userEmail, seatsFile);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			System.out.println(
					"Would you like to search for seats with specific requirements?(Y/N)\n(If no, all available seats will be printed with various features)");
			String search = Choice(console);
			String windowChoice;
			boolean windowChoiceY;
			String aisleChoice;
			boolean aisleChoiceY;
			String tableChoice;
			boolean tableChoiceY;

			if (search.toUpperCase().startsWith("Y")) {
				searchSeats.clear();
				System.out.println("Would you like a window seat? Y/N");
				windowChoice = Choice(console);
				windowChoiceY = YesNo(windowChoice, console, allSeats, availableSeats, reservedSeats, userEmail);
				System.out.println("Would you like a Aisle seat? Y/N");
				aisleChoice = Choice(console);
				aisleChoiceY = YesNo(aisleChoice, console, allSeats, availableSeats, reservedSeats, userEmail);
				System.out.println("Would you like a seat with a table? Y/N");
				tableChoice = Choice(console);
				tableChoiceY = YesNo(tableChoice, console, allSeats, availableSeats, reservedSeats, userEmail);
				System.out.println("\nSeat Number | Seat Type | Window | Aisle | Table | Price ");
				for (Seats checkAll : allSeats) {
					if (checkAll.email.equals("free") && checkAll.window == windowChoiceY
							&& checkAll.aisle == aisleChoiceY && checkAll.table == tableChoiceY) {
						System.out.printf("%7s%12s%12s%8s%8s%5s%.2f\n", checkAll.seatNum, checkAll.seatType,
								checkAll.window, checkAll.aisle, checkAll.table, pound, checkAll.seatPrice);
						searchSeats.add(checkAll);
					}
				}
				if (searchSeats.size() == 0) {
					System.out.println("There are no seats that match these requirements");
					System.out.println(
							"Would you like to quit the system?(Y/N)?\n(If no, you will be returned to the menu)");
					String quitYN = Choice(console);
					boolean quitY = YesNo(quitYN, console, allSeats, availableSeats, reservedSeats, userEmail);
					if (quitY == true) {
						Quit(allSeats, seatsFile);
					} else {
						Menu(console, allSeats, availableSeats, reservedSeats, userEmail, seatsFile);
					}
				}
				boolean reserved = false;
				do {
					System.out.println("\nPlease select from the available seats by typing the seat number:");
					String reserveChoice = Choice(console);
					String reserveChoiceVal = reserveChoice.toUpperCase().replace(" ", "");
					for (Seats viewAll : searchSeats) {
						if (viewAll.seatNum.equals(reserveChoiceVal)) {
							for (Seats checkAll : allSeats) {
								if (checkAll.seatNum.equals(reserveChoiceVal)) {
									checkAll.email = userEmail;
									System.out.println("Seat reservation successful.");
									reserved = true;
								}
							}
						}
					}
				} while (!(reserved));
			}

			else {
				boolean reserved = false;
				do {
					System.out.println("Here are all of the available seats: \n"
							+ "\nSeat Number | Seat Type | Window | Aisle | Table | Price ");
					for (Seats viewAll : availableSeats) {
						System.out.printf("%7s%12s%12s%8s%8s%5s%.2f\n", viewAll.seatNum, viewAll.seatType,
								viewAll.window, viewAll.aisle, viewAll.table, pound, viewAll.seatPrice);
					}
					System.out.println("\nPlease select from the available seats by typing the seat number:");
					String reserveChoice = Choice(console);
					String reserveChoiceVal = reserveChoice.toUpperCase().replace(" ", "");

					for (Seats checkAll : availableSeats) {
						if (checkAll.seatNum.equals(reserveChoiceVal)) {
							for (Seats viewAll : allSeats) {
								if (viewAll.seatNum.equals(reserveChoiceVal)) {
									viewAll.email = userEmail;
									reserved = true;
									System.out.println("Seat reservation successful.");
									Reserve(allSeats, availableSeats, reservedSeats, userEmail, console, pound,
											seatsFile);
								}
							}
						}
					}
				} while (!(reserved));
			}

		}
		System.out.println("There are no available seats.\n");
		System.out.println("Would you like to quit the system?(Y/N)?\n(If no, you will be returned to the menu)");
		String quitYN = Choice(console);
		boolean quitY = YesNo(quitYN, console, allSeats, availableSeats, reservedSeats, userEmail);
		if (quitY == true) {
			Quit(allSeats, seatsFile);
		} else {
			Menu(console, allSeats, availableSeats, reservedSeats, userEmail, seatsFile);
		}
	}

	public static ArrayList<Seats> AvailableSeats(ArrayList<Seats> allSeats, ArrayList<Seats> availableSeats,
			char pound) {
		for (Seats viewAll : allSeats) {
			if (viewAll.email.equals("free")) {
				availableSeats.add(viewAll);
			}
		}
		return availableSeats;

	}

	public static void Cancel(ArrayList<Seats> allSeats, ArrayList<Seats> availableSeats,
			ArrayList<Seats> reservedSeats, String userEmail, char pound, Scanner console, String seatsFile)
			throws IOException {
		System.out.println("-- CANCEL SEAT--\n");
		for (Seats viewAll : allSeats) {
			if (viewAll.email.equals(userEmail) && (!(reservedSeats.contains(viewAll)))) {
				reservedSeats.add(viewAll);
			}
		}

		String cancelYN;
		boolean cancelY = false;

		while (reservedSeats.size() > 0) {

			do {
				System.out.println(
						"Would you like to cancel a seat reservation?(Y/N)?\n(If no, you will be returned to the menu)");
				cancelYN = Choice(console);
				if (cancelYN.toUpperCase().startsWith("N")){
					Menu(console, allSeats, availableSeats, reservedSeats, userEmail, seatsFile);
				}
				if (cancelYN.toUpperCase().startsWith("Y")){
					cancelY = true;
				}
			} while (!(cancelY));

			if (cancelYN.toUpperCase().startsWith("N")) {
				Menu(console, allSeats, availableSeats, reservedSeats, userEmail, seatsFile);
			}

			System.out.println("\nSeat Number | Seat Type | Window | Aisle | Table | Price ");
			PrintReservedSeats(allSeats, reservedSeats, userEmail, pound);
			System.out.println("\nPlease select a seat to cancel by typing the seat number: ");
			String cancelChoice = Choice(console);
			String cancelChoiceVal = cancelChoice.toUpperCase().replace(" ", "");
			if (reservedSeats.toString().contains(cancelChoiceVal)) {
				for (Seats viewAll : allSeats) {
					if (viewAll.seatNum.equals(cancelChoiceVal)) {
						viewAll.email = "free";
						reservedSeats.remove(viewAll);
						System.out.printf("Seat %s cancelled\n", cancelChoiceVal);
					}
				}

			}
		}
		System.out.println("You have no reserved seats.");
		System.out.println("Enter M or Menu if you would like to return to the menu.\n"
				+ "Enter Q or Quit if you would like to quit.");
		String mqChoice = Choice(console);
		if (mqChoice.toUpperCase().startsWith("M")) {
			Menu(console, allSeats, availableSeats, reservedSeats, userEmail, seatsFile);
		}
		if (mqChoice.toUpperCase().startsWith("Q")) {
			Quit(allSeats, seatsFile);
		}
	}

	public static ArrayList<Seats> PrintReservedSeats(ArrayList<Seats> allSeats, ArrayList<Seats> reservedSeats,
			String userEmail, char pound) {
		for (Seats viewAll : allSeats) {
			if (viewAll.email.equals(userEmail)) {
				System.out.printf("%7s%12s%12s%8s%8s%5s%.2f\n", viewAll.seatNum, viewAll.seatType, viewAll.window,
						viewAll.aisle, viewAll.table, pound, viewAll.seatPrice);
			}

		}
		return reservedSeats;
	}

	public static void View(ArrayList<Seats> allSeats, ArrayList<Seats> availableSeats, ArrayList<Seats> reservedSeats,
			Scanner console, String userEmail, char pound, String seatsFile) throws IOException {
		System.out.println("-- VIEW BOOKINGS --\n");
		System.out.println(
				"Here are all of your reserved seats:\n\nSeat Number | Seat Type | Window | Aisle | Table | Price");
		PrintReservedSeats(allSeats, reservedSeats, userEmail, pound);
		System.out.println("\nEnter M or Menu if you would like to return to the menu.\n"
				+ "Enter Q or Quit if you would like to quit.");
		String mqChoice = Choice(console);
		if (mqChoice.toUpperCase().startsWith("M")) {
			try {
				Menu(console, allSeats, availableSeats, reservedSeats, userEmail, seatsFile);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (mqChoice.toUpperCase().startsWith("Q")) {
			Quit(allSeats, seatsFile);
		}
	}

	public static void Quit(ArrayList<Seats> allSeats, String seatsFile) throws IOException {
		System.out.println("-- Quit --\n (System Terminated. Seat reservations saved)");
		PrintWriter writer = null;
		try {
			FileWriter fw = new FileWriter(seatsFile, false);
			writer = new PrintWriter(fw);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		for (Seats viewAll : allSeats) {
			writer.println(viewAll.seatNum + " " + viewAll.seatType + " " + viewAll.window + " " + viewAll.aisle + " "
					+ viewAll.table + " " + viewAll.seatPrice + " " + viewAll.email);
		}
		writer.close();
		System.exit(0);
	}

	public static String Choice(Scanner console) {
		String choice = console.nextLine();
		return choice;
	}

	public static boolean YesNo(String YN, Scanner console, ArrayList<Seats> allSeats, ArrayList<Seats> availableSeats,
			ArrayList<Seats> reservedSeats, String userEmail) {

		if (YN.toUpperCase().startsWith("Y")) {
			return true;
		}
		if (YN.toUpperCase().startsWith("N")) {
			return false;
		}
		return false;
	}
}
