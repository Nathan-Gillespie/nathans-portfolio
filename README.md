# Nathans-Portfolio

A description of a selection of projects I have worked on in University and what features they have in chronological order:

**Seat Reservation System (First Year - Introduction to Programming)**
Here I created a train seat reservation system utilising objects and instantiating them to create a seat with a range of attributes. Each seat is read in from a .txt file and instantiated as an object. The user can enter their email, with some basic validation, and then begin to use the system. 
The user is presented with options in the system: reserve a seat, cancel a reservation, or view their current reservations. 
The user can choose to filter their preferred seat to find one near a window, with a table or on the aisle. 
After they are done, the user can quit the program and all reservations or cancellations will be saved, assigning their email to that seat. 

**Game and Console Web Application (First Year - Introduction to Internet and Web Development)**
I was tasked to develop an interative and dynamically generated web application to display games and information on the PS4 console. I used PHP and HTML to create the desired web pages and generate them as and when their were requested. I also used CSS for styling the web pages by changing some of the attributes of HTML tags to create a more interesting web application.

**CalcAlpha Interpretter (Second Year - Programming Language Theory)**
CalcAlpha was a very basic level lanuguage used to perform mathematical calculations with the assistance of assignments. 
The program is a fully functioning interpretter that can lexically analyse user inputted lexemes to be broken into tokens. These tokens then go through the parsing phase where they are assigned to variables to then be used in the final calculation. 
The interpretter has a range of functionality, allowing for basic arithmetic as well as more complex examples such as modulus or differences. 
The interpretter can read in strings of instructions from the user, given that they follow the correct context free grammar. 
An example input would be:
                            INT x = 4; FLOAT y = 4.5f; : x * y $
It is worth noting that calculations are performed in postfix notation.

**Hygiene Rating System (Second Year - Data Structures and Algorithms)**
This system was one developed as part of a 5-member team. I completed varying tasks of documentation, integration and testing. 
The system reads in CSV files of local authorities and can then be used to display to the user businesses that meet the criteria they request. Some functions include displaying businesses in a specific local authority as well as displaying businesses with ratings above or below a specified rating. 
The aim of this project was to create a sub-optimal program with the same functionality, to then optimise using Big O notation to understand the time complexity of the algorithms. 
