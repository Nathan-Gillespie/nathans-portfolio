<?php
// ----INCLUDE APIS------------------------------------
// Include our Website API
include ("api/api.inc.php");

// ----PAGE GENERATION LOGIC---------------------------
function createPage()
{
    $action = appFormActionSelf();
    $tcontent = <<<PAGE
                <form id="signin" method="post">
    <div>
            <div class="form-group center_div">
  <label class=" control-label" for="textinput">Username</label>
     <input id="username" name="username" type="text" placeholder="username" class="form-control input-md ">
</div>
    <div class="form-group  center_div">
     <label class=" control-label" for="passwordinput">Password</label>
     <input id="password" name="password" type="password" placeholder="password" class="form-control input-md ">
    </div>
    <div class="form-group  center_div">
     <label class=" control-label" for="passwordinput">Confirm Password</label>
     <input id="confirmpassword" name="confirmpassword" type="password" placeholder="confirm password" class="form-control input-md ">
    </div>
<div class="row">
        <button type="submit" class="btn btn-primary center-block">Enter</button>
        </div>
</div>
</form>
PAGE;
    return $tcontent;
}

// ----BUSINESS LOGIC---------------------------------
session_start();
$tpagecontent = "";
if (appFormMethodIsPost()) {
    $username = $_REQUEST["username"] ?? ProcessFormData("");
    $password = $_REQUEST["password"] ?? ProcessFormData("");
    $confirm = $_REQUEST["confirmpassword"] ?? ProcessFormData("");
    $user = new BLLUser($username, $password);

    $tvalid = true;

    if ($tvalid) {
        $uid = jsonNextUserID();
        $user->id = $uid;
        $saveuser = json_encode($user) . PHP_EOL;
        $file = "data/users.json";
        $currentfile = file_get_contents($file);
        $currentfile .= $saveuser;
        file_put_contents($file, $currentfile);
        $tpagecontent = "Thanks for making an account {$user->username}!";
    }
} else {
    $tpagecontent = createPage();
}
// ----BUILD OUR HTML PAGE----------------------------
// Create an instance of our Page class
$tindexpage = new MasterPage("Sign Up", "");
$tindexpage->setDynamic2($tpagecontent);
$tindexpage->renderPage();

?>