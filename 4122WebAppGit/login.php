<?php
// ----INCLUDE APIS------------------------------------
// Include our Website API
include ("api/api.inc.php");

// ----PAGE GENERATION LOGIC---------------------------
function createPage($method, $action, $formdata)
{
    nullAsEmpty($formdata, "username");
    nullAsEmpty($formdata, "password");
    nullAsEmpty($formdata, "err-username");
    nullAsEmpty($formdata, "err-password");

    $tcontent = <<<PAGE
<form id="signin" method="$method" $action="$action">
<div>
<div class="form-group center_div">
<label class=" control-label" for="textinput">Username</label>
<input id="username" name="username" type="text" placeholder="username" value="{$formdata["username"]}" class="form-control input-md ">
{$formdata["err-username"]}
</div>
<div class="form-group center_div">
<label class=" control-label" for="passwordinput">Password Input</label>
<input id="password" name="password" type="password" placeholder="password" value="{$formdata["password"]}" class="form-control input-md ">
{$formdata["err-password"]}
</div>
<div class="row">
<button type="submit" class="btn btn-primary center-block">Enter</button>
</div>
</div>
</form>
PAGE;
    return $tcontent;
}

function createResponse(array $formdata)
{
    $tresponse = <<<RESPONSE
        <p>You are now logged in!</p>
RESPONSE;
    return $tresponse;
}

function processForm(array $formdata): array
{
    foreach ($formdata as $field => $value) {
        $formdata[$field] = processFormData($value);
    }

    $tvalid = true;
    if ($tvalid && empty($formdata["username"])) {
        $tvalid = false;
        $formdata["err-username"] = "<p id=\"help-username\" class=\"help-block\">Username Required</p>";
    }
    if ($formdata["username"])
        if ($tvalid && empty($formdata["password"])) {
            $tvalid = false;
            $formdata["err-password"] = "<p id=\"help-password\" class=\"help-block\">Password Required</p>";
        }
    if ($tvalid) {
        $formdata["valid"] = true;
    }

    return $formdata;
}

// ----BUSINESS LOGIC---------------------------------

$paction = htmlspecialchars($_SERVER["PHP_SELF"]);
$pmethod = "POST";
$formdata = processForm($_REQUEST) ?? array();

if (isset($formdata["valid"])) {
    session_start();
    $tpagecontent = createResponse($formdata);
    $username = $_REQUEST["username"];
    $tlogintoken = $_SESSION["myuser"] ?? "";

    if (empty($tlogintoken) && ! empty($username)) {
        $_SESSION["myuser"] = processRequest($username);
        $_SESSION["entered"] = true;
        header("Location: index.php");
    } else {
        $terror = "app_error.php";
        header("Location: {$terror}");
    }
} else {
    $tpagecontent = createPage($pmethod, $paction, $formdata);
}

// ----BUILD OUR HTML PAGE----------------------------
// Create an instance of our Page class
$tindexpage = new MasterPage("Login", "");
$tindexpage->setDynamic2($tpagecontent);
$tindexpage->renderPage();

?>