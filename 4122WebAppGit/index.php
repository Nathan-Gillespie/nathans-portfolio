<?php
// ----INCLUDE APIS------------------------------------
// Include our Website API
include ("api/api.inc.php");

// ----PAGE GENERATION LOGIC---------------------------
function createPage($file)
{
    $filecontents = file_get_contents($file);
    $allgames = dalfactoryLoadAllGamesJSON($file);
    $gamesarray = dalfactoryGamesArray($allgames);
    $summaryhtml = renderSummary($gamesarray);

    $tcontent = <<<PAGE

            {$summaryhtml}
PAGE;
    return $tcontent;
}

// ----BUSINESS LOGIC---------------------------------
$file = "data/games.json";
$tpagecontent = createPage($file);

// ----BUILD OUR HTML PAGE----------------------------
// Create an instance of our Page class
$tindexpage = new MasterPage("Home", "Here's 3 Random Picks!");
$tindexpage->setDynamic2($tpagecontent);
$tindexpage->renderPage();

?>