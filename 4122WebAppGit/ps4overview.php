<?php
// ----INCLUDE APIS------------------------------------
// Include our Website API
include ("api/api.inc.php");

// ----PAGE GENERATION LOGIC---------------------------
function createPage()
{
    $console = dalfactoryConsole();
    $overviewhtml = renderConsoleOverview($console);
    $tcontent = <<<PAGE
        {$overviewhtml}
PAGE;
    return $tcontent;
}

// ----BUSINESS LOGIC---------------------------------
$tpagecontent = createPage();

// ----BUILD OUR HTML PAGE----------------------------
// Create an instance of our Page class
$tindexpage = new MasterPage("PS4 Overview", "Sony's Latest Console");
$tindexpage->setDynamic2($tpagecontent);
$tindexpage->renderPage();

?>