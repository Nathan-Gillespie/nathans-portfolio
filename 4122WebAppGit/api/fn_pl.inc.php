<?php
require_once ("oo_bll.inc.php");
// require_once ("oo_pl.inc.php");

// ALL GAME PRESENTATIONS
function renderGameRow($g, $r, $next)
{
    $class = "";
    if ($next == 1) {
        $class = "<td class=\"td-gold\">";
        $classdesc = "<td class=\"td-gold td-desc\">";
    }

    if ($next == 2) {
        $class = "<td class=\"td-silver\">";
        $classdesc = "<td class=\"td-silver td-desc\">";
    }
    if ($next == 3) {
        $class = "<td class=\"td-bronze\">";
        $classdesc = "<td class=\"td-bronze td-desc\">";
    }
    if ($next > 3) {
        $class = "<td>";
        $classdesc = "<td class=\"td-desc\">";
    }

    $trow = <<<GROW
    <tr>
    {$class}<a href="gameview.php?id={$g->id}"><img class="img-table img-responsive border-pic-table" src="img/games/{$g->image}" alt="{$g->title}"></a><a href="gameview.php?id={$g->id}"><center><strong>{$g->title}</strong></center></a></td>
    {$classdesc}{$g->description}</td>
    {$class}{$r->score}</td>
    {$classdesc}{$r->review}</td>
    </tr>
GROW;
    return $trow;
}

function renderGamesTable($allgames, $reviews)
{
    $top3 = "";
    $rows = 0;
    $next = 0;
    $rowdata = "";
    $rank = 10;
    while ($rows < 10) {
        foreach ($reviews->reviewslist as $r) {
            if ($r->score == $rank) {
                foreach ($allgames->gameslist as $g) {
                    if ($g->id == $r->id && $rows < 10) {
                        $next = $next + 1;
                        $top3 .= renderGameRow($g, $r, $next);
                        $rows = $rows + 1;
                    }
                }
            }
        }
        $rank = $rank - 1;
    }
    $table = <<<TABLE
<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th>Title</th>
							<th class="th-desc">Description</th>
							<th>Editor Score</th>
							<th class="th-desc">Editor Review</th>
						</tr>
					</thead>
					<tbody>
                    {$top3}
					{$rowdata}
					</tbody>
</table>
TABLE;
    return $table;
}

function renderSummary($allgames)
{
    $summary = "";
    // for ($x = 0; $x < 2; $x ++) {
    $x = - 1;
    $last = - 1;
    $check = 0;
    while ($check < 3) {
        $randomnum = random_int(0, 9);
        if ($randomnum != $last && $randomnum != $x) {
            $game = $allgames["$randomnum"];
            $last = $randomnum;
            if ($check == 0) {
                $x = $last;
            }
            $check = $check + 1;

            $summary .= <<<SUMM

		            <div class="row row-margin-10">
			         <div class="col-md-4 ">
				       <a href="gameview.php?id={$game->id}"><img class="img-responsive border-gradient" src="img/games/{$game->image}"></a>
			         </div>
			         <div class="col-md-8 ">
				        <p><a href="gameview.php?id={$game->id}"><h2>{$game->title}</h2></a></p>
				        {$game->description}
			     </div>
		      </div>

            <hr>
SUMM;
        }
    }

    return $summary;
}

function renderAllGameView($allgames)
{
    $allgameshtml = "";
    foreach ($allgames->gameslist as $game) {

        $allgameshtml .= <<<ALL
          		            <div class="row row-margin-10">
			         <div class="col-md-4 ">
				       <a href="gameview.php?id={$game->id}"><img class="img-responsive border-gradient" src="img/games/{$game->image}"></a>
			         </div>
			         <div class="col-md-8 ">
				        <p><a href="gameview.php?id={$game->id}"><h2>{$game->title}</h2></a></p>
				        {$game->description}
			     </div>
		      </div>

            <hr>

ALL;
    }
    return $allgameshtml;
}

function renderConsoleOverview($console)
{
    $overviewhtml = <<<VIEW


    <div class="container-fluid">
    <div class="row">
    <div class="col-sm-1"></div>
    <div class="col-sm-5">
    <img class="img-responsive" src="img/carousel/ps4.jpg" height="600" width="800"></div>
    <div class="col-sm-6"><h1>{$console->name}</h1><hr>
    <h2>Purchase</h2><br><p>This console can be bought at numerous retailers such as:<br>
    <a href="https://www.currys.co.uk/gbuk/playstation-4-ps4/gaming/gaming-consoles/430_3959_31710_xx_ba00011026-bv00311277/xx-criteria.html">
    <img class="img-responsive" src="img/logo/currys.png" height="50" width="100">
    </a>
    </div>
    </div>
    <hr>
    <div class="container-fluid">
    <div class="row">
    <div class="col-md-12">
    <h2>History</h2>
    </div>
    </div>
    <div class="row">
    <div class="col-md-6"><p>{$console->history}</p></div>
		</div>
		</div>
		<hr>
				<div class="row">
			<div class="col-md-12">
				<h2>Launch Dates</h2>
			</div>
			</div>
		<div class="row">
			<div class="col-md-12"><p>{$console->releasedate}</p></div>
		</div>
		<hr>
			</div>
      <div class="text-center"><iframe width="560" height="315" src="https://www.youtube.com/embed/{$console->trailer}" allowfullscreen></iframe></div>
		</div>
		</div>
VIEW;
    return $overviewhtml;
}

function renderIndGameView($game)
{
    $reviewsfile = "data/userreviews.json";
    $alluserreviews = dalfactoryLoadAllReviewsJSON($reviewsfile);
    $gamereviews = [];
    foreach ($alluserreviews->ureviewslist as $review) {
        if ($review->gameid == $game->id) {
            $gamereviews[] = $review;
        }
    }
    if (sizeof($gamereviews) < 1) {
        $userreviews = <<<NONE
        <h3>There are no user reviews for this game</h3>
NONE;
    } else {
        $userreviews = renderReviewsSection($gamereviews);
    }
    $overviewhtml = <<<VIEW

        	<div class="container-fluid">
<div class="row row-margin-10">
			<div class="col-md-6">
				<img class="img-responsive border-gradient" src="img/games/{$game->image}">
			</div>
            <div class="col-md-3">
				<h3><strong>Release Date</strong></h3>
                <p>{$game->releasedate}
                <h3><strong>Creator</strong></h3>
                <p>{$game->creator}
                <h3><strong>Age Rating</strong></h3>
                <p><img src="img/logo/pegi{$game->agerating}.png" height="50px" width="40px">

			</div>
            <div class="col-md-3">
				<h3><strong>Genre</strong></h3>
                <p>{$game->genre}
			</div>
</div>
        <hr>
		<h3><strong>Description</strong></h3>
		<p>{$game->description}</p>
		<hr>
        	<h3><strong>Trailer</strong></h3>
      <div class="text-center"><iframe width="560" height="315" src="https://www.youtube.com/embed/{$game->trailer}" allowfullscreen></iframe></div><hr>
		</div>
        <h3><strong>User Reviews</strong></h3>
        {$userreviews}

</div>
VIEW;
    return $overviewhtml;
}

function renderReviewsSection($gamereviews)
{
    $reviewshtml = "";
    foreach ($gamereviews as $review) {

        $reviewshtml .= <<<REVIEWS
    <p><h3>{$review->uname}</p><p><strong>Score: {$review->score}</strong></h3></p>
    <p>{$review->review}</p>
    <hr>
REVIEWS;
    }
    return $reviewshtml;
}

?>