<?php

// Include our HTML Page Class
require_once ("oo_page.inc.php");

class MasterPage
{

    // -------FIELD MEMBERS----------------------------------------
    private $_htmlpage;

    // Holds our Custom Instance of an HTML Page
    private $_dynamic_1;

    // Field Representing our Dynamic Content #1
    private $_dynamic_2;

    // Field Representing our Dynamic Content #2
    private $_dynamic_3;

    // Field Representing our Dynamic Content #3
    // -------CONSTRUCTORS-----------------------------------------
    function __construct($title, $lead)
    {
        $this->_htmlpage = new HTMLPage($title, $lead);
        $this->setPageDefaults();
        $this->setDynamicDefaults($title, $lead);
    }

    // -------GETTER/SETTER FUNCTIONS------------------------------
    public function getDynamic1()
    {
        return $this->_dynamic_1;
    }

    public function getDynamic2()
    {
        return $this->_dynamic_2;
    }

    public function getDynamic3()
    {
        return $this->_dynamic_3;
    }

    public function setDynamic1($phtml)
    {
        $this->_dynamic_1 = $phtml;
    }

    public function setDynamic2($phtml)
    {
        $this->_dynamic_2 = $phtml;
    }

    public function setDynamic3($phtml)
    {
        $this->_dynamic_3 = $phtml;
    }

    public function getPage(): HTMLPage
    {
        return $this->_htmlpage;
    }

    // -------PUBLIC FUNCTIONS-------------------------------------
    public function createPage()
    {
        // Create our Dynamic Injected Master Page
        $this->setMasterContent();
        // Return the HTML Page..
        return $this->_htmlpage->createPage();
    }

    public function renderPage()
    {
        // Create our Dynamic Injected Master Page
        $this->setMasterContent();
        // Echo the page immediately.
        $this->_htmlpage->renderPage();
    }

    public function addCSSFile($pcssfile)
    {
        $this->_htmlpage->addCSSFile($pcssfile);
    }

    public function addScriptFile($pjsfile)
    {
        $this->_htmlpage->addScriptFile($pjsfile);
    }

    // -------PRIVATE FUNCTIONS-----------------------------------
    private function setPageDefaults()
    {
        $this->_htmlpage->setMediaDirectory("css", "js", "fonts", "img", "");
        $this->addCSSfile("bootstrap.yeti.css");
        $this->addCSSfile("site.css");
        $this->addScriptFile("jquery-2.2.4.js");
        $this->addScriptFile("bootstrap.js");
        $this->addScriptFile("holder.js");
    }

    private function setDynamicDefaults($title, $lead)
    {
        $tcurryear = date("Y");
        // Set the Three Dynamic Points to Empty By Default.
        $this->_dynamic_1 = <<<JUMBO
<h1>$title</h1>
<p class="lead">$lead</p>
JUMBO;
        $this->_dynamic_2 = "";
        $this->_dynamic_3 = <<<FOOTER
<p>Nathan Gillespie - LJMU CMPNGILL; {$tcurryear}</p>
FOOTER;
    }

    private function setMasterContent()
    {
        session_start();
        $logout = "app_exit.php";
        $tentry = <<<ENTRY
                    <li><a href="login.php">Login</a></li>
					<li><a href="signup.php">Sign Up</a></li>
ENTRY;
        $texit = <<<EXIT
            <li><a class="btn btn-default navbar-right" href="{$logout}?action=exit">Exit</a></li>
EXIT;

        $tauth = isset($_SESSION["entered"]) ? $texit : $tentry;

        $tmasterpage = <<<MASTER
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">

				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
					aria-expanded="false">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
			</div>
			<a class="navbar-brand">PS4 Gaming</a>
			<!--NAVBAR -->
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">

				<ul class="nav navbar-nav navbar-left">
					<li><a href="index.php">Home</a></li>
					<li><a href="rankings.php">Game Rankings</a></li>
					<li><a href="allgames.php">Games</a></li>
					<li><a href="ps4overview.php">PS4 Overview</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
                    {$tauth}
				</ul>
			</div>
		</div>
	</nav>
	<div class="jumbotron border-bottom">
		{$this->_dynamic_1}
    </div>
<div class="container-margin">
	<div class="row details">
		{$this->_dynamic_2}
    </div>
</div>
    <footer class="footer">
		{$this->_dynamic_3}
	</footer>

MASTER;
        $this->_htmlpage->setBodyContent($tmasterpage);
    }
}

?>