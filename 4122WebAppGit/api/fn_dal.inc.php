<?php

// GAME RELATED FACTORIES
function dalfactoryLoadAllGamesJSON($gamesfile)
{
    $gamedata = file($gamesfile);
    $games = [];
    foreach ($gamedata as $line) {
        $game = json_decode($line);
        $games[] = $game;
    }
    $allgames = new BLLAllGames();
    $allgames->gameslist = $games;
    return $allgames;
}

function dalfactoryLoadAllReviewsJSON($reviewsfile)
{
    $reviewdata = file($reviewsfile);
    $reviews = [];
    foreach ($reviewdata as $line) {
        $review = json_decode($line);
        $reviews[] = $review;
    }

    $allreviews = new BLLAllGames();
    $allreviews->ureviewslist = $reviews;
    return $allreviews;
}

function dalfactoryLoadAllEditorJSON($reviewsfile)
{
    $reviewdata = file($reviewsfile);
    $reviews = [];
    foreach ($reviewdata as $line) {
        $review = json_decode($line);
        $reviews[] = $review;
    }
    $alleditorreviews = new BLLAllEditorRecomendations();
    $alleditorreviews->reviewslist = $reviews;
    return $alleditorreviews;
}

function dalfactoryGamesArray($allgames): array
{
    $array = [];
    foreach ($allgames->gameslist as $g) {
        $array[] = $g;
    }
    return $array;
}

function dalfactoryConsole(): BLLConsole
{
    $ps4 = new BLLConsole("PlayStation 4", "The PlayStation 4 (officially abbreviated as PS4) is an eighth-generation
                            home video game console developed by Sony Interactive Entertainment. Announced as the successor
                            to the PlayStation 3 in February 2013. Back in 2012 many rumors surfaced about Sony's upcoming console,
                            the PlayStation 4. The rumors suggested that it would be always-connected and wouldn't allow used games
                            similar to how the Xbox One was initially intended to be, before their u-turn. These rumors were of course
                            false. The reported nickname for the PlayStation 4 was leaked to be \"Orbis\" which lead to speculation of the
                            consoles name being \"PlayStation Orbis (PSO)\" Some rumors even went as far as saying that Sony were going
                            to ditch the DualShock altogether in favor of a Xbox like controller with PlayStation Move built in", " it
                            was launched on November 15 in North America, November 29 in Europe, South America and Australia, and on
                            February 22, 2014 in Japan.", "Curry's", "currys.png", "https // www.currys.co.uk/gbuk/playstation-4-
                            ps4/gaming/gaming-consoles/430_3959_31710_xx_ba00011026-bv00311277/xx-criteria.html", "PSVR", "The
                            PlayStation VR (officially abbreviated as PS VR ), known by the codename Project Morpheus during
                            development, is a virtual reality headset developed by Sony Computer Entertainment, which was released
                            in October 2016. It was designed to be fully functional with the PlayStation 4 home video game console.", "Jw8HCFZkBds");
    return $ps4;
}

function jsonNextUserID()
{
    return jsonNextID("data/users.json");
}

function jsonNextID($pfile)
{
    $tsplfile = new SplFileObject($pfile);
    $tsplfile->seek(PHP_INT_MAX);
    return $tsplfile->key() + 1;
}

?>