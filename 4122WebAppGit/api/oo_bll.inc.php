<?php

// GAME RELATED CLASSES
class BLLGame
{

    // -------CLASS FIELDS------------------
    public $id;

    public $title;

    public $description;

    public $agerating;

    public $releasedate;

    public $creator;

    public $genre;

    public $trailer;

    public $image;

    // -------CONSTRUCTOR--------------------
    public function __construct($gid, $gtitle, $gdesc, $gage, $grelease, $gcreator, $ggenre, $gtrailer, $gimage)
    {
        $this->id = $gid;
        $this->title = $gtitle;
        $this->description = $gdesc;
        $this->agerating = $gage;
        $this->releasedate = $grelease;
        $this->creator = $gcreator;
        $this->genre = $ggenre;
        $this->trailer = $gtrailer;
        $this->image = $gimage;
    }
}

class BLLAllGames
{

    public $gameslist;

    public function __construct()
    {
        $this->gameslist = array();
    }
}

class BLLAllEditorRecomendations
{

    public $reviewslist;

    public function __construct()
    {
        $this->reviewslist = array();
    }
}

class BLLEditorRecomendation
{

    // -------CLASS FIELDS------------------
    public $id;

    public $score;

    public $review;

    // -------CONSTRUCTOR--------------------
    public function __construct($eid, $escore, $ereview)
    {
        $this->id = $eid;
        $this->score = $escore;
        $this->review = $ereview;
    }
}

class BLLConsole
{

    // -------CLASS FIELDS------------------
    public $name;

    public $history;

    public $releasedate;

    public $buysite;

    public $sitelogo;

    public $buylink;

    public $vr;

    public $vrdesc;

    public $trailer;

    // -------CONSTRUCTOR--------------------
    public function __construct($cname, $history, $crelease, $cbuysite, $csitelogo, $cbuylink, $cvr, $cvrdesc, $ctrailer)
    {
        $this->name = $cname;
        $this->history = $history;
        $this->releasedate = $crelease;
        $this->buysite = $cbuysite;
        $this->sitelogo = $csitelogo;
        $this->buylink = $cbuylink;
        $this->vr = $cvr;
        $this->vrdesc = $cvrdesc;
        $this->trailer = $ctrailer;
    }
}

class BLLUser
{

    public $username;

    public $password;

    public function __construct($uname, $upassword)
    {
        $this->username = $uname;
        $this->password = $upassword;
    }
}

class BLLUserReview
{

    public $reviewid;

    public $gameid;

    public $username;

    public $review;

    public $score;

    public function __construct($id, $gameid, $uname, $review, $score)
    {
        $this->reviewid = $id;
        $this->gameid = $gameid;
        $this->username = $uname;
        $this->review = $review;
        $this->score = $score;
    }
}

class BLLAllUserReviews
{

    public $ureviewslist;

    public function __construct()
    {
        $this->ureviewslist = array();
    }
}

?>