<?php
// ----INCLUDE APIS------------------------------------
// Include our Website API
include ("api/api.inc.php");

// ----PAGE GENERATION LOGIC---------------------------
function createPage($games)
{
    $gameoverview = "";
    foreach ($games as $g) {
        $gameoverview = renderIndGameView($g);
    }

    $tcontent = <<<PAGE
            {$gameoverview}
PAGE;
    return $tcontent;
}

// ----BUSINESS LOGIC---------------------------------

$file = "data/games.json";
$filecontents = file_get_contents($file);
$allgames = dalfactoryLoadAllGamesJSON($file);
$games = [];
$gid = $_REQUEST["id"] ?? - 1;

if (is_numeric($gid) && $gid > 0) {
    foreach ($allgames->gameslist as $game) {
        if ($gid == $game->id) {
            $games[] = $game;
            break;
        }
    }
}

if (count($games) <= 0) {
    header("Loation: app_error.php");
    return;
}

$tpagecontent = createPage($games);

// ----BUILD OUR HTML PAGE----------------------------
// Create an instance of our Page class
$tindexpage = new MasterPage("$game->title", "");
$tindexpage->setDynamic2($tpagecontent);
$tindexpage->renderPage();

?>