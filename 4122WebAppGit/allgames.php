<?php
// ----INCLUDE APIS------------------------------------
// Include our Website API
include ("api/api.inc.php");

// ----PAGE GENERATION LOGIC---------------------------
function createPage($file)
{
    $filecontents = file_get_contents($file);
    $allgames = dalfactoryLoadAllGamesJSON($file);
    $allgameshtml = renderAllGameView($allgames);

    $tcontent = <<<PAGE

        {$allgameshtml}
PAGE;
    return $tcontent;
}

// ----BUSINESS LOGIC---------------------------------
$gamesfile = "data/games.JSON";
$pagecontent = createPage($gamesfile);
// ----BUILD OUR HTML PAGE----------------------------
// Create an instance of our Page class
$tindexpage = new MasterPage("All Games", "Have a Look!");
$tindexpage->setDynamic2($pagecontent);
$tindexpage->renderPage();

?>