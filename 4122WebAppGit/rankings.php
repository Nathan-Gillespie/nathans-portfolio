<?php
// ----INCLUDE APIS------------------------------------
// Include our Website API
include ("api/api.inc.php");

// ----PAGE GENERATION LOGIC---------------------------
function createPage($gfile, $efile)
{
    $allgames = dalfactoryLoadAllGamesJSON($gfile);
    $ereviewslist = dalfactoryLoadAllEditorJSON($efile);
    $gamestabhtml = renderGamesTable($allgames, $ereviewslist);

    $tcontent = <<<PAGE
        <div class="container-fluid">
		<h2>Game Rankings</h2>
		<hr>
		<p>Note: Rankings are purely subjective, these are based off of
			averages. You're thoughts are different? Why not make a review? We'd
			love to hear your feedback on the titles!</p>
		{$gamestabhtml}
	</div>
PAGE;
    return $tcontent;
}

// ----BUSINESS LOGIC---------------------------------
$gamesfile = "data/games.json";
$editorfile = "data/editorreviews.json";
$tpagecontent = createPage($gamesfile, $editorfile);

// ----BUILD OUR HTML PAGE----------------------------
// Create an instance of our Page class
$tindexpage = new MasterPage("Game Rankings", "Our Top 10 PS4 Titles!");
$tindexpage->setDynamic2($tpagecontent);
$tindexpage->renderPage();

?>